<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKeyWordsField extends Migration
{
    public function up()
    {
        Schema::table('article', function (Blueprint $table) {
            $table->string("keywords", 255)->nullable();
        });
    }

    public function down()
    {
        Schema::table('article', function (Blueprint $table) {
        });
    }
}
