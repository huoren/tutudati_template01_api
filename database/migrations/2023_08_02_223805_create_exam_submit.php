<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamSubmit extends Migration
{
    public function up()
    {
        Schema::create('ex_exam_submit', function (Blueprint $table) {
            $table->bigInteger("id", true, true);
            $table->bigInteger("uid", false, true)->unique("uid");
            $table->bigInteger("user_uid", false, true)->index("user_uid")->comment("用户uid");
            $table->bigInteger("collection_uid", false, true)->index("collection_uid")->comment("试卷uid");
            $table->decimal("score", 6)->default(0.00)->comment("答题总分");
            $table->integer("sort", false, true)->default(0);
            $table->tinyInteger("is_show", false, true)->default(1);
            $table->dateTime("created_at");
            $table->dateTime("updated_at");
            $table->dateTime("deleted_at")->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('ex_exam_submit');
    }
}
