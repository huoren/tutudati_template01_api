<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExCollectionField extends Migration
{
    public function up()
    {
        Schema::table('ex_collection', function (Blueprint $table) {
            $table->string("mini_qr_code", 255)->nullable()->comment("微信小程序二维码");
        });
    }

    public function down()
    {
        Schema::dropIfExists('ex_collection');
    }
}
