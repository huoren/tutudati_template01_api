<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSeoField extends Migration
{
    public function up()
    {
        Schema::table('article_cate', function (Blueprint $table) {
            $table->string("seo_title", 255)->comment("描述");
            $table->string("seo_keywords", 255)->comment("描述");
            $table->string("seo_description", 255)->comment("描述");
        });
    }

    public function down()
    {
        Schema::table('article_cate', function (Blueprint $table) {
            //
        });
    }
}
