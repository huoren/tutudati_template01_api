<!DOCTYPE html>
<html lang="zh-CN">

<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta http-equiv="Cache-Control" content="no-siteapp"/>
    <meta name="renderer" content="webkit">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    @if(!empty($content))
        <title>{{env("APP_SITE_NAME")}} - {{$content["title"]}}</title>
        <meta name="description"
              content="{{$content["remark"]}}">
        <meta name="keywords"
              content="{{$content["remark"]}}">
    @else
        <title>{{env("APP_SITE_NAME")}} - 程序员面试题宝典</title>
        <meta name="description"
              content="兔兔答题是一款，专做程序员面试答题，丰富的面试资料。包含Java，Go，PHP，Rust，Python，前端，运维，人工智能，大数据等相关面试真题的微信小程序。">
        <meta name="keywords"
              content="Java面试题，JavaScript面试题，PHP面试题，MySQL面试题，算法面试题，人工智能面试题，微信小程序答题，答题教育系统，答题小程序">
    @endif
    <link rel="shortcut icon" href="{{env("APP_URL")}}image/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="{{env("APP_URL")}}image/logo.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="{{env("APP_URL")}}image/logo.png"/>
    <meta content='{{env("APP_SITE_NAME")}}' name='Author'/>
    <link href="{{env("APP_URL")}}web/css/style.css" rel="stylesheet" type="text/css"/>

    <meta name='robots' content='max-image-preview:large'/>

    <style type="text/css">
        img.wp-smiley, img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 0.07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>

<body class="home blog">
<div class="box-wrap">
    {{--    左侧菜单导航开始--}}
    <div class="header">
        <div class="logo-wrap">
            <a href="{{env("APP_URL")}}" title="专做程序员面试题" class="logo-img-wrap">
                <img src="{{env("APP_URL")}}image/logo.png" alt="兔兔答题" class="logo">
                <h1>兔兔答题</h1>
            </a>
            <div class="sub-title">专做程序员面试题</div>
        </div>

        <div class="menu-header-container">
            <ul id="menu-header" class="menu">
                <li id="menu-item-23"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category  menu-item-23">
                    <a title="程序面试题" href="{{env("APP_URL")}}">首页</a>
                </li>
                <li id="menu-item-25"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category current-menu-item menu-item-25">
                    <a title="程序技术文章" href="{{env("APP_URL")}}article/list">答题</a>
                </li>
                <li id="menu-item-3667"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-3667">
                    <a title="程序员交流" href="{{env("APP_URL")}}article/list">资讯</a>
                </li>
                <li id="menu-item-24" class="menu-item menu-item-type-taxonomy menu-item-object-category menu-item-24">
                    <a title="程序员推荐" href="{{env("APP_URL")}}article/list">推荐</a>
                </li>
            </ul>
        </div>

        <div class="wx-code">
            <i class="tficon icon-qrcode"></i>
            扫码答题
            <img src="{{env("WEB_MENU_LOGO")}}" alt="兔兔答题微信小程序"
                 class="wx-code-img">
        </div>
    </div>
    {{--    左侧菜单导航结束--}}

    {{--    主体内容开始--}}
    <div class="main">
        <div class="top-bar">
            <div class="crumbs"><a href="{{env("APP_URL")}}">首页</a>
                <h2>试题详情</h2></div>
            <form class="search-form" method="get" action="{{env("APP_URL")}}collection/list/0">
                <button class="tficon icon-search" type="submit"></button>
                <input type="text" name="s" class="search-input" placeholder="输入关键词，回车搜索" value="">
            </form>
        </div>
        <!-- 横条广告 -->
        {{--        <ins class="adsbygoogle"--}}
        {{--             style="display:inline-block;min-width:400px;max-width:720px;width:100%;height:260px"--}}
        {{--             data-ad-client="ca-pub-4401169466922752"--}}
        {{--             data-ad-slot="8614034451"--}}
        {{--             data-ad-format="auto"--}}
        {{--             data-full-width-responsive="true"></ins>--}}
        {{--        <script>--}}
        {{--            (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--        </script>--}}
        @if(count($content))
            <div class="post-content" id="post-content" style="padding-top: 20px;border-bottom: none;">
                <img src="{{$content["url"]}}{{$content["path"]}}"
                     class="wp-post-image" alt="{{$content["title"]}}"
                     width="100%" height="auto" style="border-radius: 10px;">
                <li id="menu-item-3664"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category  current-menu-item menu-item-3664">
                    试卷名称: {{$content["title"]}}
                </li>
                <li id="menu-item-3664"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category  current-menu-item menu-item-3664">
                    试卷分类: {{$content["category"]["title"]}}
                </li>
                <li id="menu-item-3664"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category  current-menu-item menu-item-3664">
                    试卷作者: {{$content["author"]}}
                </li>
                <li id="menu-item-3664"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category  current-menu-item menu-item-3664">
                    答题时长: {{$content["time"]}}
                </li>
                <li id="menu-item-3664"
                    class="menu-item menu-item-type-taxonomy menu-item-object-category  current-menu-item menu-item-3664">
                    试卷难度: @if($content["level"] == 1)
                        简单
                    @elseif($content["level"] == 2)
                        中等
                    @elseif($content["level"] == 3)
                        较难
                    @endif
                </li>
                <button class="custom-btn btn-15 btn" data-type="alert">立即答题</button>
                <p class="single-excerpt">{{$content["remark"]}}</p>
                {!! $content["content"] !!}
                <!-- 横条广告 -->
                {{--                <ins class="adsbygoogle"--}}
                {{--                     style="display:inline-block;min-width:400px;max-width:720px;width:100%;height:260px"--}}
                {{--                     data-ad-client="ca-pub-4401169466922752"--}}
                {{--                     data-ad-slot="8614034451"--}}
                {{--                     data-ad-format="auto"--}}
                {{--                     data-full-width-responsive="true"></ins>--}}
                {{--                <script>--}}
                {{--                    (adsbygoogle = window.adsbygoogle || []).push({});--}}
                {{--                </script>--}}
            </div>
        @else
            <div class="list-empty">
                试题内容不存在
            </div>
        @endif

    </div>
    {{--主体内容结束--}}

    {{--    侧边栏开始--}}
    <div class="aside">

        <div class="aside-block">
            <h2 class="block-title">
                <i class="tficon icon-fire-line"></i> 推荐试卷
            </h2>

            <div class="sidebar-post-list">
                @foreach($relList as $value)
                    <div class="sider-post-item">
                        <a class="sider-post-item-img" href=""
                           title="{{$value["title"]}}">
                            <img class="hover-scale"
                                 src="{{$value["url"]}}{{$value["path"]}}"
                                 alt="{{$value["title"]}}">
                        </a>
                        <a class="sider-post-item-title" href="{{env("APP_URL")}}collection/detail/{{$value["uid"]}}"
                           title="{{$value["title"]}}">
                            <h3>{{$value["title"]}}</h3>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="wwads-cn wwads-horizontal" data-id="195" style="max-width:416px"></div>

        {{--        <div class="aside-block aside-ddd block-wrap">--}}

        {{--            <!-- 侧栏竖条广告 -->--}}
        {{--            <ins class="adsbygoogle aside-ddd-clum"--}}
        {{--                 style="display:block"--}}
        {{--                 data-ad-client="ca-pub-4401169466922752"--}}
        {{--                 data-ad-slot="4195151876"--}}
        {{--                 data-ad-format="auto"--}}
        {{--                 data-full-width-responsive="true"></ins>--}}
        {{--            <script>--}}
        {{--                (adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--            </script>--}}
        {{--        </div>--}}

    </div>
    {{--    侧边栏结束--}}

    {{--    试卷小程序二维码弹窗开始--}}
    <div id="app">
        <link href="https://cdn.bootcdn.net/ajax/libs/Swiper/6.4.1/swiper-bundle.min.css" rel="stylesheet"/>
        <script src="https://cdn.bootcdn.net/ajax/libs/Swiper/6.4.1/swiper-bundle.min.js"></script>
        <script src="{{env("APP_URL")}}web/plugins/modal/coco-modal.js"></script>
        <script src="{{env("APP_URL")}}web/plugins/modal/example.js"></script>
        <!--  初始化  -->
        <script>
            coco.init();
        </script>
        <script>
            example("alert", () => {
                coco({
                    header: false,
                    footer: false,
                    top: "top",
                    width: "360px",
                    html: `<div><img src='{{$content['mini_qr_code']}}' alt="{{$content["title"]}}"></div>`
                });
            });
        </script>
        <script>
            example("args", () => {
                coco({
                    text: `
          let initOptions = {
              maskClose: true,
              header: true,
              footer: true,
              title: '提示',
              text: '',
              width: '500px',
              top: '15vh',
              inputAttrs: false,
              escClose: true,
              okButton: true,
              cancelButton: true,
              okText: '确定',
              cancelText: '取消',
              closeButton: true,
              html: '',
              zIndexOfModal: 1995,
              zIndexOfMask: 2008,
              zIndexOfActiveModal: 2020,
              autoFocusOkButton: true,
              autoFocusInput: true,
              fullScreen: false,
              borderRadius: '6px',
              blur: false,
              buttonColor: '#4285ff',
              hooks: {
                open() {},
                opened() {},
                close() {},
                closed() {},
              },
              destroy: false,
              animation: false
          }
          `,
                    top: "center",
                    cancelButton: false,
                    okText: "关闭",
                    title: "所有参数初始值",
                    maskClose: false,
                    closeButton: false,
                    buttonColor: "#ffba00",
                });
            });
        </script>
    </div>
    {{--    试卷小程序二维码弹窗结束--}}
</div>
<div class="footer">
    Copy Right©2023, All Rights Reserved. {{request()->getHost()}}
    <br class="footer-br"/>
    <a href="https://beian.miit.gov.cn/" target="_blank" ref="nofollow">蜀ICP备16032791号</a>
    @foreach(app("App\Logic\Web\Config\FriendlyService")->getList() as $value)
        <a href="{{$value['site_url']}}" target="_blank" ref="nofollow">{{$value["title"]}}</a>
    @endforeach
</div>

</body>
<script>
    var _hmt = _hmt || [];
    (function() {
        var hm = document.createElement("script");
        hm.src = "https://hm.baidu.com/hm.js?1ce8a30e5d1be9b36c3c252cce920a49";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(hm, s);
    })();
</script>

</html>
