<?php

use App\Admin\Controllers\Article\ArticleController as ArticleController;
use App\Admin\Controllers\Article\CateController as ArticleCateController;
use App\Admin\Controllers\Config\AppController as ConfigAppController;
use App\Admin\Controllers\Config\BannerController as ConfigBannerController;
use App\Admin\Controllers\Config\DocumentController as ConfigDocumentController;
use App\Admin\Controllers\Config\FriendlyController;
use App\Admin\Controllers\Config\MenuController as ConfigMenuController;
use App\Admin\Controllers\Config\NoticeController as ConfigNoticeController;
use App\Admin\Controllers\Config\ProfessionController as ConfigProfessionController;
use App\Admin\Controllers\Document\DocumentController;
use App\Admin\Controllers\Document\GroupController;
use App\Admin\Controllers\Exam\AllExamController;
use App\Admin\Controllers\Exam\CategoryController as ExamCategoryController;
use App\Admin\Controllers\Exam\CollectionController as ExamCollectionController;
use App\Admin\Controllers\Exam\JudeController as ExamJudeController;
use App\Admin\Controllers\Exam\OptionController as ExamOptionController;
use App\Admin\Controllers\Exam\ReadingController as ExamReadingController;
use App\Admin\Controllers\HomeController;
use App\Admin\Controllers\Member\VipConfigController as MemberConfigController;
use App\Admin\Controllers\Message\TemplateConfigController as AdminTemplateConfigController;
use App\Admin\Controllers\Message\TemplateController as AdminTemplateController;
use App\Admin\Controllers\Resource\CategoryController as ResourceCategoryController;
use App\Admin\Controllers\Resource\SourceController as AdminResourceController;
use App\Admin\Controllers\User\UserController;
use Encore\Admin\Facades\Admin;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
Admin::routes();

Route::group([
    'prefix' => config('admin.route.prefix'),
    //    'namespace'     => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
    'as' => config('admin.route.prefix') . '.',
], function (Router $router) {
    $router->get('/', [HomeController::class, "index"])->name('home');
    $router->resource("user", UserController::class);

    // 配置管理
    $router->resource("ac/config/profession", ConfigProfessionController::class);
    $router->resource("ac/config/banner", ConfigBannerController::class);
    $router->resource("ac/config/menu", ConfigMenuController::class);
    $router->resource("ac/config/doc", ConfigDocumentController::class);
    $router->resource("ac/config/app", ConfigAppController::class);
    $router->resource("ac/config/notice", ConfigNoticeController::class);
    $router->resource("ac/friendly/link", FriendlyController::class);

    // 用户管理
    $router->resource("ac/user/list", UserController::class);

    // 会员管理
    $router->resource("user/member/config", MemberConfigController::class);

    // 考试管理
    $router->resource("ex/category", ExamCategoryController::class);
    $router->resource("ex/collection", ExamCollectionController::class);
    $router->resource("ex/reading", ExamReadingController::class);
    $router->resource("ex/option", ExamOptionController::class);
    $router->resource("ex/jude", ExamJudeController::class);
    $router->resource("ex/all", AllExamController::class);

    // 文章管理
    $router->resource("ar/cate", ArticleCateController::class);
    $router->resource("ar/article", ArticleController::class);

    // 帮助文档
    $router->resource("document/cate", GroupController::class);
    $router->resource("document", DocumentController::class);

    // 微信消息
    $router->resource("message/template", AdminTemplateController::class);
    $router->resource("message/config", AdminTemplateConfigController::class);

    // 资源
    $router->resource("resource/category", ResourceCategoryController::class);
    $router->resource("resource", AdminResourceController::class);
});
