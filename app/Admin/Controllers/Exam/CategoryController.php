<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Exam;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Exam\AdminCategory;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryController extends BaseController
{
    protected $title = "试题类目";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminCategory());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "分类名称");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->between('created_at', "创建时间")->datetime();
                $filter->equal('is_home', "首页推荐")->select([
                    1 => '是',
                    2 => '否',
                ]);
            });
        });
        $grid->model()->orderByDesc("id");
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("cover", "分类封面")->lightbox(['width' => 20, 'height' => 20]);
        $grid->column("title", "分类名称")->editable();
        $grid->column("sort", "显示权重")->integer();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");
        $grid->column("seo_title", "SEO标题")->editable();
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminCategory());
        $form->hidden("uid", "分类编号")->default(SnowFlakeId::getId());
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->image("path", "分类封面")->uniqueName();
        $form->select("parent_uid", "上级分类")->default(0)->options(AdminCategory::getFirstList());
        $form->text("title", "分类名称")->rules('required|max:20');
        $form->number("sort", "显示权重")->default(0)->help("值越大，显示权重越高");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->text("seo_title", "SEO标题")->required()->rules('required|max:100')->help("字符长度最大不能超过100");
        $form->textarea("seo_keywords", "SEO关键词")->required()->rules('required|max:255')->help("字符长度最大不能超过255");
        $form->textarea("seo_description", "SEO描述")->required()->rules('required|max:255')->help("字符长度最大不能超过255");

        return $form;
    }
}
