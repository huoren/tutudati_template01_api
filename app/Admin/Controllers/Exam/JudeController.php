<?php

declare(strict_types=1);

namespace App\Admin\Controllers\Exam;

use App\Admin\Controllers\BaseController;
use App\Model\Admin\Exam\AdminJude;
use Encore\Admin\Grid;
use Encore\Admin\Form;
use App\Model\Admin\Exam\AdminCategory;
use App\Model\Admin\Exam\AdminCollection;
use App\Library\SnowFlakeId;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class JudeController extends BaseController
{
    protected $title = "判断试题";

    public function grid()
    {
        $grid = new Grid(new AdminJude());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "试题问题");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->equal("type", "试题类型")->checkbox([1 => "单选试题", 2 => "多选试题"]);
                $filter->between('created_at', "创建时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id")->paginate(20);
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("category.title", "试题分类");
        //$grid->column("collection.title", "试卷名称");
        $grid->column("title", "试题题干");
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column("sort", "显示权重")->integer();
        $grid->column("score", "试题积分");
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminJude());
        $form->hidden("uid", "试卷编号")->default(SnowFlakeId::getId());
        $form->radio("category_uid", "试题分类")->options(AdminCategory::getList())->required();
        $form->multipleSelect("collection", "试题试卷")->options(AdminCollection::getList());
        $form->text("title", "试题题干")->rules('required|max:77')->help("最大字符长度77");
        $form->table('option', "试题选项", function ($form) {
            $form->radio('key1', "答案")->options([1 => "是", 2 => "否"])->default(2)->rules('required');
            $form->text('option', "选项")->default("正确");
        });
        $form->decimal("score", "试题积分")->help("试题积分不能小于0")->required()->value(5);
        $form->number("sort", "显示权重")->default(0)->help("值越大，显示权重越高");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->UEditor('analysis', "试题解析")->options(['initialFrameHeight' => 400]);
        $form->hidden("is_correct", "是否为答案")->default(0);
        $form->saving(function (Form $form) {
            $options = $form->option;
            foreach ($options as $key => $value) {
                if ($value["key1"] == 1) {
                    $form->is_correct = (int)$key;// 第几个选项是正确答案，该值就是第几项
                    break;
                }
            }
        });

        return $form;
    }
}
