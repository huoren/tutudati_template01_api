<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;

class BaseController extends AdminController
{
    protected $switch = [
        'on'  => ['value' => 1, 'text' => '上架', 'color' => 'primary'],
        'off' => ['value' => 2, 'text' => '下架', 'color' => 'default'],
    ];

    protected $rec = [
        'on'  => ['value' => 1, 'text' => '推荐', 'color' => 'primary'],
        'off' => ['value' => 2, 'text' => '普通', 'color' => 'default'],
    ];

    protected $isFree = [
        'on'  => ['value' => 1, 'text' => '收费', 'color' => 'primary'],
        'off' => ['value' => 2, 'text' => '免费', 'color' => 'default'],
    ];

    protected $bannerOption = [
        "min_home" => "小程序首页",
        "mini_collection_home" => "小程序试卷主页",
        "web_home" => "PC端首页",
        "mini_file_classify" => "小程序文件分类页",
        "mini_file_list" => "小程序文件列表",
    ];
}
