<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Resource;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Resource\AdminCategory;
use App\Model\Admin\Resource\AdminResource;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class SourceController extends BaseController
{
    protected $title = "资源管理";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminResource());
        $grid->filter(function ($filter) {
            $filter->disableIdFilter();
            $filter->column(1 / 2, function ($filter) {
                $filter->like('title', "资源名称");
                $filter->equal('is_show', "上架状态")->select([
                    1 => '上架',
                    2 => '下架',
                ]);
            });
            $filter->column(1 / 2, function ($filter) {
                $filter->equal('category_uid', "资源分类")->select(AdminCategory::getSecondList());
                $filter->between('created_at', "创建时间")->datetime();
            });
        });
        $grid->model()->orderByDesc("id");
        $grid->column("uid", "数据编号")->copyable();
        $grid->column("cover", "资源封面")->lightbox(['width' => 100, 'height' => 100]);
        $grid->column("category.title", "资源分类");
        $grid->column("title", "资源名称")->editable();
        $grid->column("source", "资源来源")->editable();
        $grid->column("sort", "显示权重")->integer()->sortable();
        $grid->column('is_show', "上架状态")->switch($this->switch);
        $grid->column('is_free', "是否免费")->switch($this->isFree);
        $grid->column('price', "资源价格");
        $grid->column('download_count', "下载次数");
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminResource());
        $form->hidden("uid", "资源编号")->default(SnowFlakeId::getId());
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->image("path", "资源封面")->uniqueName();
        $form->select("category_uid", "资源类目")->default(0)->options(AdminCategory::getSecondList());
        $form->text("source", "资源来源")->default("兔兔答题")->required()->help("来源最大字符长度不能超过20");
        $form->text("title", "资源名称")->help("名称最大字符长度不能超过32")->rules('required|max:32');
        $form->textarea("remark", "资源描述")->help("描述最大字符长度不能超过100");
        $form->number("download_count", "下载次数")->default(0)->required();
        $form->text("download_url", "下载地址");
        $form->number("sort", "显示权重")->default(0)->help("值越大，显示权重越高");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->switch("is_free", "是否收费")->default(2)->states($this->isFree);
        $form->decimal("price", "资源价格")->default(0.00);
        $form->UEditor('content', "资源介绍")->options(['initialFrameHeight' => 400])->required();

        return $form;
    }
}
