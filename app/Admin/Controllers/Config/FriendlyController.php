<?php
declare(strict_types=1);

namespace App\Admin\Controllers\Config;

use App\Admin\Controllers\BaseController;
use App\Library\SnowFlakeId;
use App\Model\Admin\Config\AdminFriendlyLink;
use Encore\Admin\Form;
use Encore\Admin\Grid;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class FriendlyController extends BaseController
{
    protected $title = "站点友联";

    public function grid(): Grid
    {
        $grid = new Grid(new AdminFriendlyLink());

        $grid->paginate(15);
        $grid->model()->orderByDesc("id");
        $grid->disableFilter();

        $grid->column("uid", "数据编号")->copyable();
        $grid->column("title", "站点名称");
        $grid->column("cover", "站点图标")->lightbox(['width' => 150, 'height' => 50]);
        $grid->column("sort", "显示权重")->integer()->sortable();
        $grid->column("site_url", "跳转地址");
        $grid->column('is_show', "显示状态")->switch($this->switch);
        $grid->column("created_at", "创建时间");
        $grid->disableExport();
        $grid->actions(function ($actions) {
            $actions->disableView();
        });

        return $grid;
    }

    public function form(): Form
    {
        $form = new Form(new AdminFriendlyLink());
        $form->hidden("uid", "图片编号")->default(SnowFlakeId::getId());
        $form->text("title", "站点名称")->rules('required|max:20')->help("推荐长度不超过4个中文汉字");
        $form->url("site_url", "跳转地址")->required();
        $form->hidden("url", "图片地址")->default(env("QINIU_URL"));
        $form->number("sort", "显示权重")->default(0)->min(0)->max(999)->help("权重越高，显示越靠前");
        $form->switch("is_show", "上架状态")->states($this->switch)->default(1);
        $form->image("path", "站点图标")->uniqueName();

        return $form;
    }
}
