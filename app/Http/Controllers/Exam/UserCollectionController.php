<?php
declare(strict_types=1);

namespace App\Http\Controllers\Exam;

use App\Http\Controllers\BaseController;
use App\Http\Requests\Exam\CollectionUidValidate;
use App\Http\Requests\Exam\ReadingContentValidate;
use App\Logic\Exam\UserCollectionService;
use App\Logic\Exam\UserReadingService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserCollectionController extends BaseController
{
    public function submitCollection(CollectionUidValidate $validate): JsonResponse
    {
        $createCollectionResult = (new UserCollectionService())->submitCollection();
        if ($createCollectionResult == 2) {
            return $this->success([], "收藏成功");
        } elseif ($createCollectionResult == 1) {
            return $this->error([], "你已收藏");
        }
        return $this->error([], "收藏失败");
    }

    public function submitReadingCollection(ReadingContentValidate $validate): JsonResponse
    {
        $createCollectionResult = (new UserReadingService())->submitCollection();
        if ($createCollectionResult == 2) {
            return $this->success([], "收藏成功");
        } elseif ($createCollectionResult == 1) {
            return $this->error([], "你已收藏");
        }
        return $this->error([], "收藏失败");
    }
}
