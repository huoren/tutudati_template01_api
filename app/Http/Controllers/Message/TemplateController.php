<?php

declare(strict_types=1);

namespace App\Http\Controllers\Message;

use App\Http\Controllers\BaseController;
use App\Logic\Message\TemplateService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/25
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class TemplateController extends BaseController
{
    public function submitSubscribe(): JsonResponse
    {
        $result = (new TemplateService)->submitSubscribe();
        if ($result) {
            return $this->success();
        }
        return $this->error();
    }
}
