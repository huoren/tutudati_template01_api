<?php
declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class BaseController
{
    protected function success(array $data = [], string $msg = "请求成功"): JsonResponse
    {
        return response()->json([
            "data" => $data,
            "msg" => $msg,
            "code" => 100,
        ]);
    }

    protected function error(array $data = [], string $msg = "请求失败"): JsonResponse
    {
        return response()->json([
            "data" => $data,
            "msg" => $msg,
            "code" => 101,
        ]);
    }
}
