<?php
declare(strict_types=1);

namespace App\Http\Controllers\Web;
use App\Logic\Web\Article\ArticleService;
use App\Logic\Web\Config\BannerService;
use App\Logic\Web\Exam\CollectionService;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/27
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class IndexController
{
    public function index()
    {
        $bannerList = (new BannerService())->getList(["position" => "web_home"]);
        $collectionList = (new CollectionService())->getHomeList(request()->all());
        $articleList = (new ArticleService())->getHomeList();
        $recommendList = (new CollectionService())->getHomeRecommendList(request()->all());
        $hotList = (new CollectionService())->getHomeHotList(request()->all());
        $hotArticleList = (new ArticleService())->getHomeHotList();
        return view("web.index.index", [
            "bannerList" => $bannerList,
            "collectionList" => $collectionList,
            "articleList" => $articleList,
            "recommendList" => $recommendList,
            "hotList" => $hotList,
            "hotArticleList" => $hotArticleList
        ]);
    }
}
