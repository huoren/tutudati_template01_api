<?php
declare(strict_types=1);

namespace App\Http\Controllers\Article;
use App\Http\Controllers\BaseController;
use App\Logic\Article\CategoryService;
use Illuminate\Http\JsonResponse;

/**
 * 文章分类
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/08/06
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryController extends BaseController
{
    // 文章分类
    public function getList(): JsonResponse
    {
        return $this->success((new CategoryService())->getList());
    }
}
