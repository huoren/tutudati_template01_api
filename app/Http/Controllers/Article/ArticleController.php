<?php
declare(strict_types=1);

namespace App\Http\Controllers\Article;

use App\Http\Controllers\BaseController;
use App\Logic\Article\ArticleService;
use Illuminate\Http\JsonResponse;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/23
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class ArticleController extends BaseController
{
    /**
     * 获取文章列表
     *
     * @return JsonResponse
     */
    public function getList(): JsonResponse
    {
        return $this->success((new ArticleService())->getList());
    }

    /**
     * 获取文章详情内容
     *
     * @return JsonResponse
     */
    public function getContent(): JsonResponse
    {
        return $this->success((new ArticleService())->getContent());
    }

    /**
     * 提交文章收藏
     *
     * @return JsonResponse
     */
    public function submitCollect(): JsonResponse
    {
        $createCollectionResult = (new ArticleService)->submitCollect();
        if ($createCollectionResult == 2) {
            return $this->success([], "收藏成功");
        } elseif ($createCollectionResult == 1) {
            return $this->error([], "你已收藏");
        }
        return $this->error([], "收藏失败");
    }
}
