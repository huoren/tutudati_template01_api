<?php
declare(strict_types=1);

namespace App\Model\Common\Resource;

use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Category extends BaseModel
{
    protected $table = "resources_category";

    protected $fillable = [
        "uid",
        "parent_uid",
        "title",
        "sort",
        "is_show",
        "url",
        "path",
    ];

    public function setParentUidAttribute($value)
    {
        $this->attributes["parent_uid"] = (empty($value) ? 0 : $value);
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, "parent_uid", "uid");
    }
}
