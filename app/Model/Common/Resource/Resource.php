<?php
declare(strict_types=1);

namespace App\Model\Common\Resource;

use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Resource extends BaseModel
{
    protected $table = "resources";

    protected $fillable = [
        "uid",
        "category_uid",
        "title",
        "sort",
        "is_show",
        "url",
        "path",
        "download_count",
        "content",
        "source",
        "remark",
        "is_free",
        "price",
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, "category_uid", "uid");
    }
}
