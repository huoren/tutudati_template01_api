<?php
declare(strict_types=1);

namespace App\Model\Common\Config;

use App\Model\Common\BaseModel;
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Notice extends BaseModel
{
    protected $table = "notice";

    protected $fillable = [
        "uid",
        "title",
        "is_show",
        "sort",
        "start",
        "end",
        "navigate"
    ];

    public function getNavigateAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }
}
