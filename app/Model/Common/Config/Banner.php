<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Model\Common\Config;

use App\Model\Common\BaseModel;

class Banner extends BaseModel
{
    protected $table = "banner";

    protected $fillable = [
        "uid",
        "first_title",
        "second_title",
        "url",
        "path",
        "is_show",
        "sort",
        "navigate",
        "position",
    ];

    public function getFirstTitleAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }

    public function getSecondTitleAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }

    public function getPathAttribute($key): string
    {
        return $key . "?imageView2/2/format/webp/q/95!";
    }

    public function getNavigateAttribute($key): string
    {
        return empty($key) ? "" : $key;
    }
}
