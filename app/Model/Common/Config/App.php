<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Model\Common\Config;

use App\Model\Common\BaseModel;

class App extends BaseModel
{
    protected $table = "app";

    protected $fillable = [
        "uid",
        "title",
        "url",
        "path",
        "navigate",
        "created_at",
        "updated_at",
        "value"
    ];

    public function getValueAttribute($value)
    {
        return array_values(json_decode($value, true) ?: []);
    }

    public function setValueAttribute($value)
    {
        $this->attributes['value'] = json_encode(array_values($value));
    }
}
