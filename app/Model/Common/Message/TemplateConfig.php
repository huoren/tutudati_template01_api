<?php
declare(strict_types=1);

namespace App\Model\Common\Message;

use App\Model\Common\BaseModel;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/8/03
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class TemplateConfig extends BaseModel
{
    protected $table = "template_subscribe_config";

    protected $fillable = [
        "title",
        "uid",
        "template_id",
        "sort",
        "is_show",
    ];
}
