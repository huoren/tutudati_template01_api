<?php

declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Option extends BaseModel
{
    protected $table = "ex_option";

    protected $fillable = [
        "uid",
        "collection_uid",
        "category_uid",
        "title",
        "second_title",
        "option",
        "score",
        "answer",
        "type",
        "analysis",
        "sort",
        "is_show",
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, "category_uid", "uid");
    }

    public function collection(): BelongsToMany
    {
        return $this->belongsToMany(Collection::class, "ex_collection_option_rel",
            "exam_uid", "collection_uid", "uid", "uid");
    }

    public function getAnswerAttribute($key)
    {
        return json_decode($key, true);
    }

    public function getSecondTitleAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }

    public function getAnalysisAttribute($key): string
    {
        return !empty($key) ? $key : "";
    }

    public function getOptionAttribute($value): array
    {
        return array_values(json_decode($value, true) ?: []);
    }

    public function setOptionAttribute($value)
    {
        $this->attributes['option'] = json_encode(array_values($value));
    }
}
