<?php
declare(strict_types=1);

namespace App\Model\Common\Exam;

use App\Model\Common\BaseModel;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class Category extends BaseModel
{
    protected $table = "ex_category";

    protected $fillable = [
        "uid",
        "title",
        "is_show",
        "sort",
        "parent_uid",
        "url",
        "path",
        "seo_title",
        "seo_keywords",
        "seo_description"
    ];
}
