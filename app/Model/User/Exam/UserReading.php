<?php
declare(strict_types=1);

namespace App\Model\User\Exam;

use App\Model\Common\Exam\Reading;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class UserReading extends Reading
{

}
