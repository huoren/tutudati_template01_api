<?php
declare(strict_types=1);

namespace App\Model\Admin\Exam;

use App\Model\Common\Exam\Category;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/17
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminCategory extends Category
{
    protected $appends = [
        "cover"
    ];

    public function getCoverAttribute($key): string
    {
        return $this->getAttribute("url") . $this->getAttribute("path");
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, "parent_uid", "uid");
    }

    // 获取二级分类
    public static function getList(): array
    {
        $items = self::query()->where("is_show", "=", 1)->get(["uid", "title"]);
        $list = [];
        foreach ($items as $value) {
            $list[$value->uid] = $value->title;
        }
        return $list;
    }

    // 获取一级分类
    public static function getFirstList(): array
    {
        $items = self::query()->where([
            ["is_show", "=", 1],
        ])->whereNull("parent_uid")->get(["uid", "title"]);
        $list = [];
        foreach ($items as $value) {
            $list[$value->uid] = $value->title;
        }
        return $list;
    }
}
