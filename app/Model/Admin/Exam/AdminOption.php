<?php
declare(strict_types=1);

namespace App\Model\Admin\Exam;
use App\Model\Common\Exam\Option;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminOption extends Option
{
//    public function getCollectionUidAttribute($value)
//    {
//        return explode(',', (string)$value);
//    }

//    public function setCollectionUuidAttribute($value)
//    {
//        $this->attributes['collection_uid'] = implode(',', $value);
//    }
}
