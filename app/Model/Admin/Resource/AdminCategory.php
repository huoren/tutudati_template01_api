<?php
declare(strict_types=1);

namespace App\Model\Admin\Resource;

use App\Model\Common\Resource\Category;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/5/16
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminCategory extends Category
{
    protected $appends = [
        "cover"
    ];

    public static function getList(): array
    {
        $items = self::query()->orderByDesc("id")->where([
            ["parent_uid", "=", 0],
            ["is_show", "=", 1]
        ])->get(["uid", "title"]);
        $list = [];
        foreach ($items as $value) {
            $list[$value->uid] = $value->title;
        }

        return $list;
    }

    public static function getSecondList(): array
    {
        $items = self::query()->orderByDesc("id")->where([
            ["parent_uid", "!=", 0],
            ["is_show", "=", 1]
        ])->get(["uid", "title"]);
        $list = [];
        foreach ($items as $value) {
            $list[$value->uid] = $value->title;
        }

        return $list;
    }

    public function getCoverAttribute($key): string
    {
        return $this->getAttribute("url") . $this->getAttribute("path");
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, "parent_uid", "uid");
    }
}
