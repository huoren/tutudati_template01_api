<?php
declare(strict_types=1);

namespace App\Model\Admin\Message;

use App\Model\Common\Message\TemplateConfig;
use Illuminate\Support\Facades\DB;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/8/03
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class AdminTemplateConfig extends TemplateConfig
{
    protected $appends = [
        "count",// 总次数
        "group_count", // 总人数
    ];

    public function getCountAttribute(): int
    {
        return AdminTemplate::query()
            ->where([
                ["template_id", "=", $this->getAttributes()["template_id"]]
            ])
            ->count();
    }

    public function getGroupCountAttribute(): int
    {
        // TODO 代码优化
        $value = DB::select("select count(*) as cnt from
                           (select count(distinct user_uid) from template_subscribe where template_id = '{$this->getAttributes()["template_id"]}' group by user_uid) as a");
        return $value[0]->cnt;
    }
}
