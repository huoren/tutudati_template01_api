<?php
declare(strict_types=1);
/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/9
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */

namespace App\Model\Admin\Config;

use App\Model\Common\Config\Profession;

class AdminProfession extends Profession
{
    public static function getList(): array
    {
        $items = self::query()->get(["uid", "title"]);
        $list = [];
        foreach ($items as $value) {
            $list[$value->uid] = $value->title;
        }

        return $list;
    }
}
