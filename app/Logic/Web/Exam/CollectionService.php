<?php
declare(strict_types=1);

namespace App\Logic\Web\Exam;

use App\Model\User\Exam\UserCollection;
use Closure;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/27
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CollectionService
{
    private function searchWhere(array $requestParams): Closure
    {
        return function ($query) use ($requestParams) {
            $query->where("is_show", "=", 1);
            if (!empty($requestParams["category_uid"])) {
                $query->where("category_uid", "=", $requestParams["category_uid"]);
            }
            if (!empty($requestParams["hot"])) {
                $query->orderByDesc("collection");
            }
            if (!empty($requestParams["recommend"])) {
                $query->where("recommend", "=", $requestParams["recommend"]);
            }
            if (!empty($requestParams["title"])) {
                $query->where("title", "like", "%{$requestParams['title']}%");
            }
            if (empty($requestParams["recommend"]) && empty($requestParams["hot"])) {
                $query->orderByDesc("sort");
            }
        };
    }

    // 获取首页试卷列表
    public function getHomeList(array $requestParams): array
    {
        return UserCollection::query()
            ->with(["category:uid,title"])
            ->where($this->searchWhere($requestParams))
            ->select(["uid", "url", "path", "title", "time", "collect", "submit", "category_uid", "level", "author", "remark", "created_at"])
            ->orderByDesc("id")
            ->limit($requestParams["size"] ?? 20)
            ->get()
            ->toArray();
    }

    // 首页推荐试卷列表
    public function getHomeRecommendList(array $requestParams): array
    {
        $requestParams["recommend"] = 1;
        return UserCollection::query()
            ->with(["category:uid,title"])
            ->where($this->searchWhere($requestParams))
            ->select(["uid", "url", "path", "title", "category_uid"])
            ->orderByDesc("id")
            ->limit($requestParams["size"] ?? 5)
            ->get()
            ->toArray();
    }

    // 首页热门试卷列表
    public function getHomeHotList(array $requestParams): array
    {
        return UserCollection::query()
            ->with(["category:uid,title"])
            ->where($this->searchWhere($requestParams))
            ->select(["uid", "url", "path", "title", "category_uid"])
            ->orderByDesc("collect")
            ->limit($requestParams["size"] ?? 3)
            ->get()
            ->toArray();
    }

    // 首页试卷列表
    public function getList(array $requestParams): LengthAwarePaginator
    {
        return UserCollection::query()
            ->with(["category:uid,title"])
            ->where($this->searchWhere($requestParams))
            ->select(["uid", "url", "path", "title", "category_uid", "created_at", "author", "level", "remark"])
            ->orderByDesc("collect")
            ->paginate(10);
    }

    // 获取热门试卷
    public function getHostList(array $requestParams): LengthAwarePaginator
    {
        $requestParams["recommend"] = 1;
        return UserCollection::query()
            ->with(["category:uid,title"])
            ->where($this->searchWhere($requestParams))
            ->select(["uid", "url", "path", "title", "category_uid"])
            ->orderByDesc("collect")
            ->paginate(8);
    }

    // 试卷内容
    public function getContent(array $requestParams): array
    {
        $bean = UserCollection::query()
            ->with(["category:uid,title"])
            ->where("uid", "=", $requestParams["uid"])
            ->where("is_show", "=", 1)
            ->first(["content", "uid", "url", "path", "title", "time", "collect", "submit", "category_uid", "level", "author", "remark", "mini_qr_code"]);
        if (!empty($bean)) {
            $bean = $bean->toArray();
            $bean["content"] = str_replace("text-wrap: nowrap;", "", $bean["content"]);
            return $bean;
        }
        return [];
    }

    // 查询试卷详情页相关的推荐试卷
    public function getContentRecommendList(array $requestParams): array
    {
        $bean = UserCollection::query()->where("uid", "=", $requestParams["uid"])->first(["category_uid"]);
        if (!empty($bean)) {
            return UserCollection::query()
                ->with(["category:uid,title"])
                ->where([
                    ["category_uid", "=", $bean->category_uid],
                    ["is_show", "=", 1],
                    ["recommend", "=", 1]
                ])
                ->select(["uid", "url", "path", "title", "category_uid"])
                ->orderByDesc("id")
                ->limit($requestParams["size"] ?? 5)
                ->get()
                ->toArray();
        }
        return [];
    }
}
