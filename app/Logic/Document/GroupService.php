<?php
declare(strict_types=1);

namespace App\Logic\Document;

use App\Logic\BaseUserService;
use App\Model\User\Document\UserGroup;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class GroupService extends BaseUserService
{
    public function getList(): array
    {
        return UserGroup::query()
            ->with(["document:uid,title,group_uid"])
            ->where([
                ["is_show", "=", 1]
            ])->orderByDesc("sort")
            ->get(["title", "uid"])
            ->toArray();
    }
}
