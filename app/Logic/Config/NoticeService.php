<?php
declare(strict_types=1);

namespace App\Logic\Config;

use App\Logic\BaseUserService;
use App\Model\User\Config\UserNotice;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/22
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class NoticeService extends BaseUserService
{
    public function getList(): array
    {
        $currentDateTime = date("Y-m-d H:i:s");
        return UserNotice::query()->where([
            ["is_show", "=", 1],
            ["start", "<=", $currentDateTime],
            ["end", ">=", $currentDateTime],
        ])->get(["title", "navigate"])->toArray();
    }
}
