<?php
declare(strict_types=1);

namespace App\Logic\Exam;

use App\Logic\BaseUserService;
use App\Model\User\Exam\UserCategory;
use Closure;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/19
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class CategoryService extends BaseUserService
{
    private function searchWhere(): Closure
    {
        return function ($query) {
            $requestParams = request()->all();
            extract($requestParams);
            $query->where("is_show", "=", 1);
        };
    }

    // 获取分类全量列表
    public function getList(): array
    {
        $requestParams = request()->all();
        $perSize = $requestParams["size"] ?? 20;
        return UserCategory::query()
            ->where($this->searchWhere())
            ->orderByDesc("id")
            ->limit($perSize)
            ->get(["title", "uid"])
            ->toArray();
    }
}
