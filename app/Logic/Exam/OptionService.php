<?php

declare(strict_types=1);

namespace App\Logic\Exam;

use App\Logic\BaseUserService;
use App\Model\User\Exam\UserCollectionOptionRel;
use App\Model\User\Exam\UserOption;

/**
 * @project: 兔兔考试系统
 * @author: Mandy
 * @date: 2023/7/24
 * @link: https://www.tutudati.com/
 * @site: 微信搜索-兔兔考试系统
 */
class OptionService extends BaseUserService
{
    // 获取试卷所有选择题
    public function getList(): array
    {
        $requestParams = request()->all();
        $examUid = UserCollectionOptionRel::query()
            ->where("collection_uid", "=", $requestParams["collection_uid"])
            ->get(["exam_uid"])
            ->toArray();
        if (empty($examUid)) {
            return [];
        }
        $examUidArray = array_column($examUid, "exam_uid");
        return UserOption::query()
            ->whereIn("uid", $examUidArray)
            ->where([
                ["is_show", "=", 1],
            ])
            ->orderByDesc("sort")
            ->orderByDesc("id")
            ->get(["uid", "type", "option", "score", "analysis", "answer", "title", "second_title"])
            ->toArray();
    }
}
